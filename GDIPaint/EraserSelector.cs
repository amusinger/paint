﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GDIPaint
{
    public partial class EraserSelector : Form
    {
        public string eraserType = "0";
        public EraserSelector()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            eraserType = "small";
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
     

        }

        private void button2_Click(object sender, EventArgs e)
        {
            eraserType = "medium";
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
     
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            eraserType = "big";
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
     
          
        }
    }
}
