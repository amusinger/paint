﻿using SimpePaint;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace GDIPaint
{

    public partial class Form1 : Form
    {
        MyPaint paint = null;
        Point firstPoint = Point.Empty;
        Point secondPoint = Point.Empty;

        public Form1()
        {
            InitializeComponent();
            paint = new MyPaint(pictureBox1.Size);
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(paint.bitmap, Point.Empty);
            switch (paint.tool)
            {
                case Tool.Line:
                    e.Graphics.DrawLine(paint.p, firstPoint, secondPoint);
                    break;

                case Tool.Rectangle:
                    e.Graphics.DrawRectangle(paint.p, Math.Min(firstPoint.X, secondPoint.X), Math.Min(firstPoint.Y, secondPoint.Y), Math.Abs(secondPoint.X - firstPoint.X), Math.Abs(secondPoint.Y - firstPoint.Y));
                    break;

                case Tool.Circle:
                    e.Graphics.DrawEllipse(paint.p, Math.Min(firstPoint.X, secondPoint.X), Math.Min(firstPoint.Y, secondPoint.Y), Math.Abs(secondPoint.X - firstPoint.X), Math.Abs(secondPoint.Y - firstPoint.Y));
                    break;

                default:
                    break;
            }


        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            firstPoint = e.Location;
            if (paint.tool == Tool.Fill)
            {
                Color c = paint.bitmap.GetPixel(e.X, e.Y);

                MapFill mf = new MapFill();

                mf.Fill(paint.g, e.Location, paint.p.Color, ref paint.bitmap);

                pictureBox1.Refresh();

            }
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            secondPoint = e.Location;

            switch (paint.tool)
            {
                case Tool.Line:
                    paint.g.DrawLine(paint.p, firstPoint, secondPoint);
                    break;
                case Tool.Rectangle:
                    paint.g.DrawRectangle(paint.p, Math.Min(firstPoint.X, secondPoint.X), Math.Min(firstPoint.Y, secondPoint.Y), Math.Abs(secondPoint.X - firstPoint.X), Math.Abs(secondPoint.Y - firstPoint.Y));
                    break;
                case Tool.Circle:
                    paint.g.DrawEllipse(paint.p, Math.Min(firstPoint.X, secondPoint.X), Math.Min(firstPoint.Y, secondPoint.Y), Math.Abs(secondPoint.X - firstPoint.X), Math.Abs(secondPoint.Y - firstPoint.Y));
                    break;
                default:
                    break;
            }

            this.Refresh();

        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {

            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                secondPoint = e.Location;
                this.Refresh();
            }
            switch (paint.tool)
            {
                case Tool.Brush:
                    HatchBrush myHatchBrush = new HatchBrush(HatchStyle.Vertical, paint.p.Color, Color.White);
                    paint.p.Brush = myHatchBrush;
                    GraphicsPath graphPath = new GraphicsPath();
                    graphPath.AddEllipse(secondPoint.X - 10, secondPoint.Y - 10, 20, 20);
                    paint.g.FillPath(paint.p.Brush, graphPath);
                    break;

                case Tool.Brush1:
                    SolidBrush simpleBrush = new SolidBrush(paint.p.Color);
                    paint.p.Brush = simpleBrush;
                    GraphicsPath graphPath1 = new GraphicsPath();
                    graphPath1.AddEllipse(secondPoint.X - 10, secondPoint.Y - 10, 20, 20);
                    paint.g.FillPath(paint.p.Brush, graphPath1);
                    break;

                case Tool.Brush2:
                    HatchBrush myHatchBrush2 = new HatchBrush(HatchStyle.SmallConfetti, paint.p.Color, Color.White);
                    paint.p.Brush = myHatchBrush2;
                    GraphicsPath graphPath4 = new GraphicsPath();
                    graphPath4.AddEllipse(secondPoint.X - 10, secondPoint.Y - 10, 20, 20);
                    paint.g.FillPath(paint.p.Brush, graphPath4);
                    break;


                case Tool.Pen:
                    paint.g.DrawLine(paint.p, firstPoint, secondPoint);
                    firstPoint = secondPoint;
                    break;

                case Tool.Eraser:
                    GraphicsPath graphPath2 = new GraphicsPath();
                    Rectangle rect = new Rectangle(secondPoint.X - 10, secondPoint.Y - 10, 20, 20);
                    graphPath2.AddRectangle(rect);
                    paint.p.Color = Color.White;
                    paint.g.FillPath(paint.p.Brush, graphPath2);
                    break;

                case Tool.Eraser1:
                    GraphicsPath graphPath3 = new GraphicsPath();
                    Rectangle rect2 = new Rectangle(secondPoint.X - 5, secondPoint.Y - 5, 10, 10);
                    graphPath3.AddRectangle(rect2);
                    paint.p.Color = Color.White;
                    paint.g.FillPath(paint.p.Brush, graphPath3);
                    break;

                case Tool.Eraser2:
                    GraphicsPath graphPath5 = new GraphicsPath();
                    Rectangle rect3 = new Rectangle(secondPoint.X - 15, secondPoint.Y - 15, 30, 30);
                    graphPath5.AddRectangle(rect3);
                    paint.p.Color = Color.White;
                    paint.g.FillPath(paint.p.Brush, graphPath5);
                    break;

            }
            this.Refresh();

        }

        private void button7_Click(object sender, EventArgs e)
        {
            Button b = sender as Button;
            paint.p.Color = b.BackColor;
            SolidBrush simpleBrush = new SolidBrush(paint.p.Color);
            paint.p.Brush = simpleBrush;
        }
        private void PenSelect(object sender, EventArgs e)
        {
            paint.tool = Tool.Pen;
            paint.p.EndCap = System.Drawing.Drawing2D.LineCap.Round;
            paint.p.StartCap = System.Drawing.Drawing2D.LineCap.Round;
        }


        private void BrushSelect(object sender, EventArgs e)
        {
           BrushSelector bs = new BrushSelector();
           if (bs.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
           {
               if (bs.brushType == "1")
               {
                   paint.tool = Tool.Brush1;
               }
               else if (bs.brushType == "2")
               {
                   paint.tool = Tool.Brush;
               }
               else if (bs.brushType == "3")
               {
                   paint.tool = Tool.Brush2;
               }
           }
           
        }



        private void EraserSelect(object sender, EventArgs e)
        {
            EraserSelector es = new EraserSelector();
            if (es.eraserType == "small")
            {
                paint.tool = Tool.Eraser1;
            }
            else if (es.eraserType == "medium")
            {
                paint.tool = Tool.Eraser;
            }
            else if (es.eraserType == "big")
            {
                paint.tool = Tool.Eraser2;
            }
        }

        private void Rectangle(object sender, EventArgs e)
        {
            paint.tool = Tool.Rectangle;
        }
       
        private void Line(object sender, EventArgs e)
        {
            paint.tool = Tool.Line;
        }
        private void Circle(object sender, EventArgs e)
        {
            paint.tool = Tool.Circle;

        }
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            paint.bitmap.Save("1.png");
        }

        private void colorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            paint.p.Color = colorDialog1.Color;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            paint.tool = Tool.Fill;
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            paint.p.Width = trackBar1.Value * 5 + 1;
        }


    }
}
